# Odin Admin Dashboard

This project was created as part of The Odin Project (TOP) curriculum. The project page can be found [here](https://www.theodinproject.com/lessons/node-path-intermediate-html-and-css-admin-dashboard#solutions).

## License
This project is provided under the MIT License, copyright 2023 David Palubin
